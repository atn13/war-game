# War Game

## An Nguyen

### Description

During a hackathon in Georgia Tech, my partner and I created an iOS game app based on the classic War card game. I learned Swift and how to put an app together in iOS, both of which I did not know prior. For this app, we each created our own War game so that we could both gain a better overall understanding of the app development process. Of course, it was also helpful since we could help each other if one got stuck at a certain part. Collaborating with another student helped me develop teamwork and communication skills.

#### Skills learned:

- iOS development
- Basics of Swift
- Storyboard usage
- User design
- Teamwork
- Communication

#### Screenshots:

*Home Screen*:

![Home Screen](images/war.png)

*Playing*:

![Playing](images/war2.png)
